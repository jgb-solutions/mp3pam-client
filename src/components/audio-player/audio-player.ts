import { ApiProvider } from './../../providers/api/api';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
  selector: 'audio-player',
  templateUrl: 'audio-player.html'
})
export class AudioPlayerComponent {
	// music controls
	// @Input() paused;

//     @Output() backward = new EventEmitter();
//     @Output() pauseplay = new EventEmitter();
//     @Output() forward = new EventEmitter();
//     @Output() random = new EventEmitter();
// 	@Output() stop = new EventEmitter();

// 	// music progress
// 	track: string;
//     // @Input() elapsed: string;
//     @Input() total: string;
// 	@Input() current: number;

// 	image: string;
// 	myTracks: any[];
//   	singleTrack: any;
//   	allTracks: any[];
//   	selectedTrack: number;
// 	title: string = 'Dènye Mizik Ou Jwe';
// 	tracks: any[] = [];
//     filteredTracks: any[] = [];
//     position;
//     elapsed;
//     duration;
// 	paused = true;
// 	music: any;

// 	constructor(
// 		private api: ApiProvider,
// 		private events: Events
// 	) {
// 		// Listen to Audio events
//         this.listenToAudioEvents();
// 		this.listenToEvents();
// 	}

// 	handleRandom() {
//         const randomTrack = this.randomTrack(this.tracks);

//         this.play(randomTrack.stream_url);
//         this.title = randomTrack.title;
//     }


//     handleEnded(e) {
//         this.handleRandom();
//     }

//     handleTimeUpdate(e) {
//         const elapsed = this.audio.currentTime;
//         const duration = this.audio.duration;
//         this.position = elapsed / duration;
//         this.elapsed = this.formatTime(elapsed);
//         this.duration = this.formatTime(duration);
//     }

//     handleQuery(payload) {
//         // this.musicService.findTracks(payload).subscribe(tracks => {
//         //     this.filteredTracks = tracks;
//         // });
//     }

//     handlePausePlay() {
//         if (this.audio.paused) {
//             this.paused = true;
//             this.audio.play();
//         } else {
//             this.paused = false;
//             this.audio.pause();
//         }
//     }

//     handleStop() {
//         this.audio.pause();
//         this.audio.currentTime = 0;
//         this.paused = false;
//     }

//     handleBackward() {
//         const elapsed = this.audio.currentTime;
//         console.log(elapsed);
//         if (elapsed >= 5) {
//             this.audio.currentTime = elapsed - 5;
//         }
//     }

//     handleForward() {
//         const elapsed = this.audio.currentTime;
//         const duration = this.audio.duration;
//         if (duration - elapsed >= 5) {
//             this.audio.currentTime = elapsed + 5;
//         }
//     }

// 	load(url) {
//         // this.audio.src = this.apiService.prepareUrl(url);
//         // this.audio.load();
//     }

//     play(url) {
//         this.load(url);
//         // this.audio.play();
//     }

//     getPlaylistTracks() {
//         return this.api.getMusic('http://mp3pam-server.test/assets/audio/placehoder-music.mp3')
//             .map(res => res.json())
//             .map(music => this.music = music);
//     }

//     randomTrack(tracks) {
//         const trackLength = tracks.length;
//         const randomNumber = Math.floor(Math.random() * trackLength + 1);
//         return tracks[randomNumber];
//     }

//     formatTime(seconds) {
//         let minutes: any = Math.floor(seconds / 60);
//         minutes = (minutes >= 10) ? minutes : '0' + minutes;
//         seconds = Math.floor(seconds % 60);
//         seconds = (seconds >= 10) ? seconds : '0' + seconds;
//         return minutes + ':' + seconds;
//     }

//     // findTracks(value) {
//     //     return this.apiService.get(`${this.apiService.prepareUrl('https://api.soundcloud.com/tracks')}&q=${value}`, false)
//     //         .debounceTime(300)
//     //         .distinctUntilChanged()
//     //         .map(res => res.json());
//     // }

// 	playSelectedTrack() {
// 		// use AudioProvider to control selected track
// 		// this.audioProvider.play(this.selectedTrack);
// 	}

// 	pauseSelectedTrack() {
// 		// use AudioProvider to control selected track
// 		// this.audioProvider.pause(this.selectedTrack);
// 	}

// 	onTrackFinished(track: any) {
// 		console.log('Track finished', track)
// 	}

// 	prepare(music) {
// 		return {
// 			src: 'http://mp3pam.dev/assets/audio/placeholder-music.mp3',
// 			artist: music.artist.name,
// 			title: music.title,
// 			art: music.image_url,
// 			preload: 'metadata' // tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
// 		}
// 	}

// 	listenToEvents() {
// 		this.events.subscribe('playTrack', (music) => {
// 			console.log('Playing track', music)
// 			this.title = music.title

// 			this.singleTrack = this.prepare(music)

// 			// this.audioProvider.play(0)

// 		})

// 		this.events.subscribe('pauseTrack', (music) => {
// 			console.log('Pausing track', music)

// 			// this.audioProvider.pause(0)
// 		})

// 		this.events.subscribe('stopTrack', (music) => {
// 			console.log('Stopping track', music)

// 			// this.audioProvider.stop(0)
// 		})
// 	}

// 	listenToAudioEvents() {
// 		this.audio.onended = this.handleEnded.bind(this);
//         this.audio.ontimeupdate = this.handleTimeUpdate.bind(this);
// 	}
}
