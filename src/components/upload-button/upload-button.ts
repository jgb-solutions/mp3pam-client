
import {Component, ElementRef, Input, Output, ViewChild, Renderer, EventEmitter} from "@angular/core"

@Component({
	selector: 'upload-button',
	templateUrl: 'upload-button.html'
})
export class UploadButtonComponent {
	@Input() icon: String
	
	@Input() buttonText: String

	@Output() filesChange = new EventEmitter()


	@ViewChild("input")
	private nativeInputBtn: ElementRef

	constructor (
		private renderer: Renderer
	) {
		console.log('hello from Upload Button Component')
	}

	public callback(event: Event): void {
		// trigger click event of hidden input
		let clickEvent: MouseEvent = new MouseEvent("click", {bubbles: true})
		this.renderer.invokeElementMethod(
			this.nativeInputBtn.nativeElement, "dispatchEvent", [clickEvent])
	}

	public filesAdded(event: Event): void {
		let files: FileList = this.nativeInputBtn.nativeElement.files
		this.filesChange.emit(files)
	}
}
