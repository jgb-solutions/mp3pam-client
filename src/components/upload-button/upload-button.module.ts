
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { UploadButtonComponent } from './upload-button';

@NgModule({
  declarations: [
    UploadButtonComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    UploadButtonComponent
  ]
})
export class UploadButtonComponentModule {}
