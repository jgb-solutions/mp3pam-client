import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { LogoHeaderComponent } from './logo-header';

@NgModule({
  declarations: [
    LogoHeaderComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    LogoHeaderComponent
  ]
})
export class LogoHeaderComponentModule {}
