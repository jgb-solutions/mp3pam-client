import { Component, Input } from '@angular/core';


@Component({
  selector: 'logo-header',
  templateUrl: 'logo-header.html'
})
export class LogoHeaderComponent {
  	@Input() title: string
  	@Input() icon: string

  	// constructor() {}
}
