
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { NavButtonsComponent } from './nav-buttons';

@NgModule({
  declarations: [
    NavButtonsComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    NavButtonsComponent
  ]
})
export class NavButtonsComponentModule {}
