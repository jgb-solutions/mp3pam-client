import { Component } from '@angular/core'
import { AudioProvider } from './../../providers/audio/audio';
import { MenuController, NavController, ModalController } from 'ionic-angular'

@Component({
  selector: 'nav-buttons',
  templateUrl: 'nav-buttons.html'
})
export class NavButtonsComponent {

	constructor(
		public menu: MenuController,
		public navCtrl: NavController,
		public modal: ModalController,
		public audioProvider: AudioProvider
	) {}

  	displayAudioPlayer() {
		this.navCtrl.push('PlayerPage');
	}

	displaySearch() {
		this.navCtrl.push('SearchPage');
	}

	displayFavorites() {
		this.navCtrl.push('FavoritesPage');
		console.log('Favorites Page');
	}

}
