
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MpSpinnerComponent } from './mp-spinner';

@NgModule({
  declarations: [
    MpSpinnerComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    MpSpinnerComponent
  ]
})
export class MpSpinnerComponentModule {}
