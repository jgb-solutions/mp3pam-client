import { Component } from '@angular/core';

@Component({
  selector: 'mp-spinner',
  templateUrl: 'mp-spinner.html'
})
export class MpSpinnerComponent {}
