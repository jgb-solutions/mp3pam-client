import { LanguageProvider } from './../../providers/language/language';
import { Component } from '@angular/core'
import { NavController, AlertController, NavParams, IonicPage } from 'ionic-angular'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

import { ApiProvider } from './../../providers/api/api';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service'
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";


@IonicPage()
@Component({
selector: 'page-login',
templateUrl: 'login.html',
})
export class LoginPage {
	title: string;
	loginForm: FormGroup;
	disableForm: boolean;
	showLoader: boolean = false;
	errorMessage: string;
	successMessage: string;
	emailPattern: any = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

	constructor(
		public navCtrl: NavController,
		public auth: AuthServiceProvider,
		public api: ApiProvider,
		public formBuilder: FormBuilder,
		public alertCtrl: AlertController,
		public params: NavParams,
		private inAppBrowser: InAppBrowser,
		public lang: LanguageProvider,
	) {
		this.title = lang.get('login');
		let errorMessage = params.get('errorMessage')
		let successMessage = params.get('successMessage')

		if (errorMessage) {
			this.errorMessage = errorMessage
		}

		if (successMessage) {
			this.successMessage = successMessage
		}

		// Create the login form
		this.loginForm = formBuilder.group({
			email: ['', Validators.compose([
				Validators.required,
				Validators.pattern(this.emailPattern)
			])],
			password: ['', Validators.required]
		})
	}

	ionViewDidLoad() {
		// console.log('ionViewDidLoad LoginPage')
		document.title = this.title
		// if (localStorage.getItem('userData')) {
		// 	alert(localStorage.getItem('userData'));
		// } else {
		// 	alert('nothing yet');
		// }
	}

	loginWithFacebook() {
		const inAppBrowserOptions: InAppBrowserOptions = {
			location: 'no',
			zoom: 'no',
			closebuttoncaption: 'Fèmen',
			// clearcache: 'yes'
		}
		let apiResponse;

		const browser = this.inAppBrowser.create(this.api.facebookAuthURL, '_blank', inAppBrowserOptions);
		browser.on('loadstop').subscribe((event) => {
			// check the url
			// if this is the api url then fetch the data from the dom
			if (event.url.indexOf(this.api.facebookAuthHandleURL) != -1) {
				browser.executeScript({ code: "document.body.innerText" })
				.then((response) => {
					// alert(response);
					apiResponse = response;
					browser.close();
				});
			} else {
				alert('wrong url');
			}
		});
		browser.on('exit').subscribe((event) => {
			// alert('browser exited yeah');
			// alert(apiResponse);
			// localStorage.setItem('userData', JSON.stringify(apiResponse));
			this.auth.loginWithFacebook(apiResponse);
			this.navCtrl.setRoot('HomePage');
		});
	}

	login() {
		console.log(this.loginForm)
		this.disableForm = true
		this.showLoader = true
		this.errorMessage = ''
		this.successMessage = ''


		console.log(this.loginForm.value)

		this.api.login(this.loginForm.value).subscribe(res => {
			console.log(res)
			this.auth.loginWith(res)
			this.navCtrl.setRoot('HomePage')
		}, error =>  {
			this.showLoader = false
			this.disableForm = false
			this.errorMessage = error.message
		})
	}

	openSignupPage() {
		this.navCtrl.setRoot('SignupPage')
	}

	openPasswordRecoveryPage() {
		this.navCtrl.setRoot('PasswordRecoveryPage')
	}

	// onError(error) {
	// 	console.log(error)

	// 	// let alert = this.alertCtrl.create({
	// 	// 	title: 'Erè Koneksyon!',
	// 	// 	subTitle: '<p>Nou pa rive mizik yo. Ta sanble entènèt ou pa bon.</p>',
	// 	// 	buttons: ['OKE']
	// 	// })

	// 	// alert.present()
	// 	this.errorMessage = error.message
	// }
}
