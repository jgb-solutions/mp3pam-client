import { LogoHeaderComponentModule } from './../../components/logo-header/logo-header.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    LogoHeaderComponentModule
  ],
  exports: [
    LoginPage
  ]
})
export class LoginPageModule {}
