import { MpSpinnerComponentModule } from './../../components/mp-spinner/mp-spinner.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadMusicPage } from './upload-music';

@NgModule({
  declarations: [
    UploadMusicPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadMusicPage),
    MpSpinnerComponentModule
  ],
  exports: [
    UploadMusicPage
  ]
})
export class UploadMusicPageModule {}
