import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  segment: 'mete-mizik'
})
@Component({
  selector: 'page-upload-music',
  templateUrl: 'upload-music.html',
})
export class UploadMusicPage {
	showLoader: boolean = true

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams
	) {

	}

	ionViewDidLoad() {
		// check if user is eligible to upload music
		// if eligible show view to choose artist
		// after choosing an artist the upload music form will shown
		// if user is not eligible an error will be shown with the reason why
		// the user is not able to upload music.
		// can be because the user has used all his/her free uploads
		// or an error from the server
		// or maybe they don't have an internet connection for now.
	}

}
