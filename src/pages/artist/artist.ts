import { MusicInterface as Music } from './../../interfaces/MusicInterface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ArtistInterface as Artist } from './../../interfaces/ArtistInterface';

@IonicPage({
	segment: 'artist/:hash'
})
@Component({
  selector: 'page-artist',
  templateUrl: 'artist.html',
})
export class ArtistPage {
	artist: Artist;
	artistDetails: string = 'bio';
	showLoader: boolean = true;
	hash: string;
	musics: Array<Music> = [];

	constructor(
		public navCtrl: NavController,
		public params: NavParams
	) {
		this.hash = params.get('hash');
		this.artist = params.get('artist');
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ArtistPage');
	}

	goToMusicDetailPage($event, music) {
		this.navCtrl.push('MusicDetailPage', { hash: music.hash,  music });
	}
}
