import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutPage } from './about';

@NgModule({
  declarations: [
    AboutPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutPage),
    NavButtonsComponentModule
  ],
  exports: [
    AboutPage
  ]
})
export class AboutPageModule {}
