import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPage } from './user';

@NgModule({
  declarations: [
    UserPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPage),
    NavButtonsComponentModule
  ],
  exports: [
    UserPage
  ]
})
export class UserPageModule {}
