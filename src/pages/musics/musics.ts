import { Http } from '@angular/http'
import { Component } from '@angular/core'
import { ApiProvider } from '../../providers/api/api'
import { NetworkProvider } from './../../providers/network/network';
import { MusicInterface as Music } from './../../interfaces/MusicInterface';
import {
	IonicPage,
	NavController,
	NavParams,
	AlertController,
	ToastController
} from 'ionic-angular';

@IonicPage({
	segment: 'mizik'
})
@Component({
selector: 'page-musics',
templateUrl: 'musics.html',
})
export class MusicsPage {
	meta: any;
	links: any;
	hasMore: boolean;
	infiniteScroll: any;
	errorMessage: string;
	musics: Array<Music>;
	lastFetchedPage: number;
	loading: boolean = false;
	canShowAlert: boolean = true
	showLoader: boolean = true;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private http: Http,
		// public loadingCtrl: LoadingController,
		public api: ApiProvider,
		public alertCtrl: AlertController,
		public toastCtrl: ToastController,
		public network: NetworkProvider
	) {
		// this.presentLoading()
		// this.getmusics()
	}

	ionViewDidLoad() {
		// Get a fresh page of musics
		this.getmusics()
	}

	getmusics(refresher?) {
		this.network.checkStatus(this, refresher);

		this.api.getMusics().subscribe(res => {
			this.musics = res.data;
			this.meta = res.meta;
			this.links = res.links;

			this.lastFetchedPage = this.meta.current_page;

			this.hasMore = this.meta.current_page < this.meta.last_page;

			console.log(res);

			if (refresher) {
				refresher.complete()
			}

			if (this.infiniteScroll) {
				this.infiniteScroll.enable(true)
			}

			this.loading = false
			this.showLoader = false;
			this.canShowAlert = true
		}, error =>  {
			this.loading = false
			this.errorMessage = error.message

			if (refresher) {
				refresher.complete()
			}

			if (this.infiniteScroll) {
				this.infiniteScroll.enable(true)
			}
		})
	}

	reload(refresher) {
		this.network.checkStatus(this, refresher)
		this.getmusics(refresher)
	}

	loadMore(infiniteScroll) {
		this.network.checkStatus(this, infiniteScroll);

		if (this.hasMore) {
			if (this.loading) return;

			console.log('fetching:' + this.links.next)

			this.infiniteScroll = infiniteScroll

			this.lastFetchedPage++

			this.loading = true;

			this.api.getMusics(this.links.next).subscribe(res => {
				this.musics = this.musics.concat(res.data);
				this.meta = res.meta;
				this.links = res.links;
				this.hasMore = this.meta.current_page < this.meta.last_page;
				this.loading = false;
				infiniteScroll.complete();
				console.log(res);
			}, error =>  {
				this.errorMessage = error.message;
			});
		}

		if (this.meta.current_page == this.meta.last_page) {
			this.toastCtrl.create({
				message: 'Ou gentan chaje tout mizik yo deja',
				duration: 5000,
				position: 'bottom',
			}).present();

			infiniteScroll.enable(false)
		}
  	}

	// canFetchMore() {
	// 	console.log(this.hasMore && this.makePageUrl(this.lastFetchedPage) != next_page)

	// 	return this.hasMore && this.makePageUrl(this.lastFetchedPage) != next_page
	// }

	makePageUrl(pageNumber: number) {
		return this.meta.path + '?page=' + pageNumber
	}

	goToMusicDetailPage($event, music) {
		this.navCtrl.push('MusicDetailPage', { hash: music.hash,  music });
	}

	// onError(error) {
	// 	console.log('error from subscription: ' + error)

	// 	let alert = this.alertCtrl.create({
	// 		title: 'Erè chajman mizik!',
	// 		subTitle: '<p>Nou pa ka chaje mizik yo. Ta sanble entènèt ou pa bon.</p>',
	// 		buttons: ['OKE']
	// 	})

	// 	alert.present()
	// }
}
