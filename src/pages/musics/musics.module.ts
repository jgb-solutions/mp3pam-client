import { MpSpinnerComponentModule } from './../../components/mp-spinner/mp-spinner.module';
import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicsPage } from './musics';

@NgModule({
  declarations: [
    MusicsPage,
  ],
  imports: [
    IonicPageModule.forChild(MusicsPage),
    NavButtonsComponentModule,
    MpSpinnerComponentModule
  ],
  exports: [
    MusicsPage
  ]
})
export class MusicsPageModule {}
