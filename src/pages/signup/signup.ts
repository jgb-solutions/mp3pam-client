import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
	title: string = 'Kreye Kont'

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams
	) {}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SignupPage');
		document.title = this.title
	}

  	openLoginPage() {
    	this.navCtrl.setRoot('LoginPage');
  	}

	signup() {

	}
}
