import { LogoHeaderComponentModule } from './../../components/logo-header/logo-header.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';

@NgModule({
  declarations: [
    SignupPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupPage),
    LogoHeaderComponentModule
  ],
  exports: [
    SignupPage
  ]
})
export class SignupPageModule {}
