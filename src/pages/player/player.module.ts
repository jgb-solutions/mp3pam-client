import { AudioPlayerComponent } from './../../components/audio-player/audio-player';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayerPage } from './player';

@NgModule({
	declarations: [
		PlayerPage,
		AudioPlayerComponent
	],
	imports: [
		IonicPageModule.forChild(PlayerPage),
	],
	exports: [
		PlayerPage
	]
})
export class PlayerPageModule {}
