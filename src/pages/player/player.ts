import { Component } from '@angular/core';
import { AudioProvider } from './../../providers/audio/audio';
import { IonicPage, Events, NavController } from 'ionic-angular';

@IonicPage({
	segment: 'player',
	defaultHistory: ['MusicsPage']
})
@Component({
	selector: 'page-player',
	templateUrl: 'player.html',
})
export class PlayerPage {
	constructor(
		public audioProvider: AudioProvider,
		private events: Events,
		public navCtrl: NavController
	) {}

	viewArtistPage(artist) {
		this.navCtrl.push('ArtistPage', {hash: artist.hash, artist});
	}
}
