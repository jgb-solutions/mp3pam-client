import { Component } from '@angular/core'
import { ApiProvider } from './../../providers/api/api'
import { SocialSharing } from '@ionic-native/social-sharing';
import { MusicInterface as Music } from './../../interfaces/MusicInterface';
import {
	Events,
	NavParams,
	IonicPage,
	NavController,
	ToastController,
} from 'ionic-angular'

@IonicPage({
	segment: 'mizik/:hash',
	defaultHistory: ['MusicsPage']
})
@Component({
  selector: 'page-music-detail',
  templateUrl: 'music-detail.html',
})
export class MusicDetailPage {
	hash: string;
	music: Music;
	isFavorited: false;
	playing: boolean = false;
	showShare: boolean = false;
	showLoader: boolean = true;
	downloaded: boolean = false;
	relatedMusics: Array<Music>;
	musicDetails: string = 'details';

	constructor(
		private events: Events,
		public params: NavParams,
		private api: ApiProvider,
		public navCtrl: NavController,
		private toastCtrl: ToastController,
		private socialSharing: SocialSharing
	) {
		this.hash = params.get('hash');
		this.music = params.get('music');
	}

	viewArtistPage(artist): void {
		this.navCtrl.push('ArtistPage', {hash: artist.hash, artist});
	}

	ionViewDidLoad() {
		// Check if the being viewed music is favorited
		this.checkIfFavorite();

		// do other stuff
		let url;

		if (! this.music) {
			url = this.api.makeUrlFor('music', this.hash);
			this.showLoader = true;
		} else {
			url = this.music.url;
		}

		this.api.getMusic(url).subscribe(res => {
			console.log(res);
			this.music = res.music;
			this.relatedMusics = res.related.data;

			this.showLoader = false;
		}, err => {
			console.log('error loading single track' + err);
		});
	}

	play(music: Music) {
		this.playing = ! this.playing;
		this.events.publish('playTrack', music);
	}

	pause(music: Music) {
		this.playing = !this.playing
		this.events.publish('pauseTrack')
	}

	// stop(music: Music) {
	// 	this.events.publish('stopTrack', music)

	// 	this.playing = ! this.playing
	// }

	download(music: Music) {
		this.downloaded = true
		window.location.href = music.download_url;

		let toast = this.toastCtrl.create({
				message: 'Telechajman an pral kòmanse. Mèsi paske w chwazi telechaje mizik sou MP3 Pam',
				duration: 5000,
				position: 'bottom'
			});

			toast.present();
	}

	checkIfFavorite() {
		// check API for whether if's favorited or not.
	}

	favorite(music: Music) {
		alert('making favorite');
	}

	unfavorite(music: Music) {
		alert('making unfovorited');
	}

	shareViaEmail(music: Music) {
		this.socialSharing.shareViaEmail(
			`${music.artist.stageName} - ${music.title} \n
			${music.image} \n
			${music.publicUrl}`,
			`${music.artist.stageName} - ${music.title}`,
			['to'],
		).then(() => {
			this.onSharedSuccess('Email');
		}).catch(() => {
			this.onSharedError('Email');
		});;
	}

	shareVia(music: Music, provider: string): void {
		this.socialSharing.canShareVia(provider.toLowerCase()).then(() => {
			this.socialSharing['shareVia' + provider](
				this.makeShareMessage(music),
				music.image,
				music.publicUrl,
			).then(() => {
				this.onSharedSuccess(provider);
			}).catch(() => {
				this.onSharedError(provider);
			});
		}, (error) => {
			this.cantShareVia(provider);
		});
	}

	onSharedError(provider: string): void {
		console.log('Nou pa rive pataje mizik la sou ' + provider);
	}

	onSharedSuccess(provider: string): void {
		console.log('Nou pataje mizik la avèk sisksè sou ' + provider);
	}

	cantShareVia(provider: string): void {
		let toast = this.toastCtrl.create({
			message: 'Fòk ou enstale ' + provider + ' sou telefòn ou an pou w kapab pataje mizik sou li.',
			duration: 3000,
			position: 'bottom',
		});

		toast.present();
	}

	makeShareMessage(music: Music): string {
		return `Al tande/telechaje \n
		"${music.artist.stageName} - ${music.title} (${music.publicUrl})" \n
		sou @MP3Pam (MP3 Pam)`;
	}

	goToSingleCategoryPage(category): void {
		this.navCtrl.push('SingleCategoryPage', { hash: category.slug, category })
	}

	goToMusicDetailPage($event, music): void {
		this.navCtrl.push('MusicDetailPage', { hash: music.hash,  music });
	}
}
