import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicDetailPage } from './music-detail';

@NgModule({
  declarations: [
    MusicDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MusicDetailPage),
    NavButtonsComponentModule
  ],
  exports: [
    MusicDetailPage
  ]
})
export class MusicDetailPageModule {}
