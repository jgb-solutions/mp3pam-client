import { LogoHeaderComponentModule } from './../../components/logo-header/logo-header.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PasswordRecoveryPage } from './password-recovery';

@NgModule({
 	declarations: [
    	PasswordRecoveryPage,
 	],
	imports: [
		IonicPageModule.forChild(PasswordRecoveryPage),
		LogoHeaderComponentModule
	],
	exports: [
		PasswordRecoveryPage
	]
})
export class PasswordRecoveryPageModule {}
