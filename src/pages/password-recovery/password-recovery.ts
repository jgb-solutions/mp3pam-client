import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ApiProvider } from './../../providers/api/api'
import { AuthServiceProvider } from './../../providers/auth-service/auth-service'
import { PasswordValidators } from './../../validators/password-validators';

@IonicPage()
@Component({
  selector: 'page-password-recovery',
  templateUrl: 'password-recovery.html',
})
export class PasswordRecoveryPage {
	title: string = 'Rekiperasyon Modpas'
	recoveryForm: FormGroup
	passwordForm: FormGroup
	verificationForm: FormGroup
	disableForm: boolean
	showLoader: boolean = false
	showRecoveryForm: boolean = true
	showPasswordForm: boolean = false
	showVerificationForm: boolean = false
	errorMessage: string
	infoMessage: string	
	recoveryCode: string
	tempEmail: string
	emailPattern: any = /^(([^<>()\[\]\.,:\s@\"]+(\.[^<>()\[\]\.,:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,:\s@\"]+\.)+[^<>()[\]\.,:\s@\"]{2,})$/i

	constructor(
		public navCtrl: NavController,
		public auth: AuthServiceProvider,
		public api: ApiProvider,
		public formBuilder: FormBuilder,
		// public alertCtrl: AlertController,
		public params: NavParams,
		public events: Events
	) {
		let errorMessage = params.get('errorMessage')

		if (errorMessage) {
			this.errorMessage = errorMessage
		}
		
		// Create the recovery form
		this.recoveryForm = formBuilder.group({
			email: ['', Validators.compose([
				Validators.required,
				Validators.pattern(this.emailPattern)
			])]
		})

		// Create the verification form
		this.verificationForm = formBuilder.group({
			code: ['', Validators.compose([
				Validators.required,
				Validators.minLength(30),
				Validators.maxLength(30)
			])]
		})

		// Create the password form
		this.passwordForm = formBuilder.group({
			password: ['', Validators.required],
			password_confirmation: ['', Validators.compose([
				Validators.required,
				PasswordValidators.Confirmation
			])]
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage')
		document.title = this.title
	}

	recover() {
		console.log(this.recoveryForm)
		this.disableForm = true
		this.showLoader = true
		this.errorMessage = ''
		
		// store the email in case of success response
		this.tempEmail = this.recoveryForm.controls.email.value
		
		this.api.recover(this.recoveryForm.value).subscribe(res => {
			this.showRecoveryForm = false
			this.showLoader = false
			this.disableForm = false
			this.showVerificationForm = true
			this.infoMessage = res.message
		}, res => this.onError(res))
	}

	verify() {
		this.disableForm = true
		this.showLoader = true
		this.errorMessage = ''
		this.infoMessage = ''

		let credentials = {
			email: this.tempEmail,
			code: this.verificationForm.controls.code.value
		}
		console.log(credentials)

		this.api.verify(credentials).subscribe(res => {
			console.log(res)
			this.infoMessage = res.message
			this.showVerificationForm = false
			this.showPasswordForm = true
			this.showLoader = false
			this.disableForm = false
		}, res =>  this.onError(res))
	}

	changePassword() {
		this.disableForm = true
		this.showLoader = true
		this.errorMessage = ''
		this.infoMessage = ''

		let credentials = {
			email: this.tempEmail,
			password: this.passwordForm.controls.password.value,
			password_confirmation: this.passwordForm.controls.password_confirmation.value,
		}

		this.api.changePassWord(credentials).subscribe(res => {
			this.events.publish('passwordChanged')
		}, res =>  this.onError(res))
	}

	openSignupPage() {
		this.navCtrl.setRoot('SignupPage')
	}

	openLoginPage() {
    	this.navCtrl.setRoot('LoginPage')
  	}

	onError(res) {
		console.log(res)
		this.showLoader = false
		this.disableForm = false

		switch (res.status) {
			case 404:
				this.errorMessage = 'Nou pa rive jwenn imel ou a. Sanble ou pa menm gen yon kont! Kreye youn.'
				break
			case 422:
				// this.errorMessage = 'Gen yon erè nan kòd ou voye a. Al kopye epi kole kòd nou te voye sou imel ou a'
				this.errorMessage = 'Gen yon erè nan fòm nan. Tanpri korije erè yo epi eseye ankò.'
				break
			default:
				this.errorMessage = res.message
				break
		}
	}
}
