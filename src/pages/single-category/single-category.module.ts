import { MpSpinnerComponentModule } from './../../components/mp-spinner/mp-spinner.module';
import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleCategoryPage } from './single-category';

@NgModule({
  declarations: [
    SingleCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleCategoryPage),
    NavButtonsComponentModule,
    MpSpinnerComponentModule
  ],
  exports: [
    SingleCategoryPage
  ]
})
export class SingleCategoryPageModule {}
