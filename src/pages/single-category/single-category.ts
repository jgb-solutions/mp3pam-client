import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage({
	segment: 'kategori/:hash',
	defaultHistory: ['CategoriesPage']
})
@Component({
  selector: 'page-single-category',
  templateUrl: 'single-category.html',
})
export class SingleCategoryPage {
	category: any
	showLoader: boolean = true
	hash: string
	errorMessage: string
	musics: Array<any> = [] 
	response: any
	hasMore: boolean
	lastFetchedPage: number
	canShowAlert: boolean = true
	infiniteScroll: any

	constructor(
		public navCtrl: NavController,
		public params: NavParams,
		public api: ApiProvider,
		public toastCtrl: ToastController
	) {
		this.category = params.get('category')
		this.hash = params.get('hash')
	}

	ionViewDidLoad() {
		let url

		if (! this.category) {
			url = this.api.makeUrlFor('category', this.hash)
			this.showLoader = true
		} else {
			url = this.category.url
		}

		this.getMusicsByCategory(url)
	}

	getMusicsByCategory(url) {
		this.api.getMusicsByCategory(url).subscribe(res => {
			console.log(res)

			this.category = res.category;
			document.title = 'Kategori: ' + res.category.name;

			this.musics = res.musics.data

			this.response = res.musics

			this.lastFetchedPage = res.musics.current_page

			this.hasMore = res.musics.current_page < res.musics.last_page

			if (this.infiniteScroll) {
				this.infiniteScroll.enable(true)
			}

			this.showLoader = false
			this.canShowAlert = true
		}, res =>  {
			this.showLoader = false
			this.errorMessage = res.message
	
			if (this.infiniteScroll) {
				this.infiniteScroll.enable(true)
			}
		})
	}

	loadMore(infiniteScroll) {
		this.infiniteScroll = infiniteScroll

		if (this.musics.length == 0) {
			this.infiniteScroll.enable(false)
		}
		
		let next_page = this.response.next_page_url

		if (this.canFetchMore(next_page)) {
			console.log('fetching:' + next_page)

			this.lastFetchedPage++

			this.api.getMusics(next_page).subscribe(res => {
				this.musics = this.musics.concat(res.musics.data)

				this.response = res.musics

				this.hasMore = res.musics.current_page < res.musics.last_page

				console.log('Async operation has ended', res)
			}, res =>  {
				this.errorMessage = res.message
			})
		}

		if (this.response.current_page == this.response.last_page) {
			this.toastCtrl.create({
				message: 'Nou gentan chaje tout mizik nan kategori ' + this.category.name + ' yo deja.',
				duration: 5000,
				position: 'bottom',
				showCloseButton: true,
				closeButtonText: 'OKE'
			}).present()

			infiniteScroll.enable(false)
		}

		infiniteScroll.complete()
  	}

	canFetchMore(next_page) {
		console.log(this.hasMore && this.makePageUrl(this.lastFetchedPage) != next_page)

		return this.hasMore && this.makePageUrl(this.lastFetchedPage) != next_page
	}

	makePageUrl(pageNumber: number) {
		return this.response.path + '?page=' + pageNumber
	}

	goToMusicDetailPage(music) {
		music['category'] = this.category
		console.log(music)
		this.navCtrl.push('MusicDetailPage', { hash: music.hash,  music })
	}

}
