import { AlgoliaProvider } from './../../providers/algolia/algolia';
import { Component } from '@angular/core';
// import { AdsProvider } from './../../providers/ads/ads';
import { NavController, IonicPage } from 'ionic-angular';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';

@IonicPage({
	segment: 'akey'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	user: any
	imageData: string
	imgSrcs: any
	imgSrc: string
	audioSrc: string

  	constructor(
		public navCtrl: NavController,
		public auth: AuthServiceProvider,
		// private ads: AdsProvider
		public algolia: AlgoliaProvider
	) {
		this.user = this.auth.user
	}

	ionViewDidLoad() {
		// this.ads.showInterstitialAd();
		// this.ads.showVideoRewardsAd();
		this.algolia.searchMusics('she');
		this.algolia.searchArtists('stark');
	}

	showBannerAd() {
		// this.ads.showBannerAd();
	}

	showInterstitialAd() {
		// this.ads.showInterstitialAd();
	}

	showRewardVideoAd() {
		// this.ads.showRewardVideoAd();
	}

	hideBannerAd() {
		// this.ads.hideBannerAd();
	}

	openMusicsPage() {
		this.navCtrl.push('MusicsPage')
	}

	getImageData(file) {
		let reader = new FileReader()

		reader.onload = () => {
			this.imgSrc = reader.result
		}

		if (file) {
			reader.readAsDataURL(file)
		}

		// return URL.createObjectURL(file)
	}

	previewAudio(file) {
		// console.log(file, reader)

		let reader = new FileReader();

		reader.onload = () => {
			this.audioSrc = reader.result
		};

		reader.readAsDataURL(file);

		this.audioSrc = URL.createObjectURL(file)
	}

	upload() {
		var formData = new FormData()

		formData.append("username", "Groucho")
		formData.append("accountnum", '123456') // number 123456 is immediately converted to a string "123456"

		// HTML file input, chosen by user
		// formData.append("userfile", fileInputElement.files[0])

		var request = new XMLHttpRequest()
		request.open("POST", "http://foo.com/submitform.php")
		request.send(formData)
	}

	getImage(files) {
		console.log(files)
		let imagesSources = []

		for (let i = 0; i < files.length; i++) {
			let file = files[i]
			console.log(file)
			console.log(this.getImageData(file))
			imagesSources.push(this.getImageData(file))
		}

		this.imgSrcs = imagesSources

		console.log(this.imgSrcs)
	}

	getAudio(files) {
		console.log(files)
		let imagesSources = []

		for (let i = 0; i < files.length; i++) {
			let file = files[i]
			console.log(file)
			console.log(this.getImageData(file))
			imagesSources.push(this.getImageData(file))
		}

		this.imgSrcs = imagesSources

		console.log(this.imgSrcs)
	}
}