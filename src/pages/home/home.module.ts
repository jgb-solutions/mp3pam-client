import { UploadButtonComponentModule } from './../../components/upload-button/upload-button.module';
import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    NavButtonsComponentModule,
    UploadButtonComponentModule,
    IonicPageModule.forChild(HomePage),
  ],
  exports: [
    HomePage
  ]
})
export class HomePageModule {}
