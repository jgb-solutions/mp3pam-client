import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { ApiProvider } from './../../providers/api/api';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoryInterface as Category } from './../../interfaces/CategoryInterface';

@IonicPage({
	segment: 'kategori'
})
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html'
})
export class CategoriesPage {
	categories: Array<Category>;
	showLoader: boolean = true;
	errorMessage: string;

	constructor(
		public navCtrl: NavController,
		public params: NavParams,
		public api: ApiProvider,
		private storage: Storage
	) {}

	ionViewDidLoad() {
		// Todo
		// Get all categories and display them
		this.getCachedCategories();
	}

	getCachedCategories() {
		this.storage.get('categories').then(cachedCategories => {
			if (cachedCategories) {
				this.categories = JSON.parse(cachedCategories);
				this.getCategories();
				this.showLoader = false;
			} else {
				// fetch categories from API
				this.getCategories();
				this.showLoader = false;
			}
		}, error => console.log(error));
	}

	getCategories() {
		// we're going to fetch the categories from the API
		this.api.getCategories().subscribe(categories => {
			// we store the response in the local property so
			// that we can show it
			this.categories = categories;

			// We store the categories in local Storage for faster load next time
			this.storage.set('categories', JSON.stringify(categories));

			// and then we hide the loader
			this.showLoader = false;
		}, errorResponse => {
			// we console log the error message if we have one.
			console.log(errorResponse.message, errorResponse.status);

			this.errorMessage = 'Gen yon erè. Tanpri eseye ankò.';
		});
	}

	goToSingleCategoryPage(category) {
		this.navCtrl.push('SingleCategoryPage', { hash: category.slug, category })
	}
}
