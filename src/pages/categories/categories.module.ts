import { MpSpinnerComponentModule } from './../../components/mp-spinner/mp-spinner.module';
import { NavButtonsComponentModule } from './../../components/nav-buttons/nav-buttons.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesPage } from './categories';

@NgModule({
  declarations: [
    CategoriesPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriesPage),
    NavButtonsComponentModule,
    MpSpinnerComponentModule
  ],
  exports: [
    CategoriesPage
  ]
})
export class CategoriesPageModule {}
