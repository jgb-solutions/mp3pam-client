import { MpSpinnerComponentModule } from './../../components/mp-spinner/mp-spinner.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchPage } from './search';

@NgModule({
  declarations: [
    SearchPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchPage),
    MpSpinnerComponentModule
  ],
  exports: [
    SearchPage
  ]
})
export class SearchPageModule {}
