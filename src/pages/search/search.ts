import {
	Content,
	NavParams,
	IonicPage,
	NavController,
	AlertController,
	ToastController,
} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Component, ViewChild } from '@angular/core';
import { ApiProvider } from './../../providers/api/api';
import { NetworkProvider } from './../../providers/network/network';
import { AlgoliaProvider } from './../../providers/algolia/algolia';
import { MusicInterface as Music } from '../../interfaces/MusicInterface';
import { ArtistInterface as Artist } from './../../interfaces/ArtistInterface';

@IonicPage()
@Component({
	selector: 'page-search',
	templateUrl: 'search.html',
})
export class SearchPage {
	searchTerm: string;
	musics: Music[] = [];
	artists: Artist[] = [];
	showBreakTags: boolean;
	showLoader: boolean = true;
	tabResults: string = 'musics';
	showSegmentTab: boolean = false;
	@ViewChild(Content) content: Content;

	constructor(
		private api: ApiProvider,
		private storage: Storage,
		public navParams: NavParams,
		public navCtrl: NavController,
		public algolia: AlgoliaProvider,
		public network: NetworkProvider,
		public alertCtrl: AlertController,
		public toastCtrl: ToastController,
	) {}

	ionViewDidLoad() {
		// fetch last results from local storage
		this.getCacheData();
	}

	getCacheData() {
		this.storage.get('results').then(cacheResults => {
			if (cacheResults) {
				const parseCacheResults = JSON.parse(cacheResults);
				this.showSegmentTab = true;
				this.musics = parseCacheResults.musics;
				this.artists = parseCacheResults.artists;
				this.searchTerm = parseCacheResults.searchTerm;
				this.showLoader = false;
				// fix the issue with the content being overlapped
				this.content.resize();
			} else {
				this.showLoader = false;
				this.showBreakTags = true;
			}
			console.log(JSON.parse(cacheResults));
		});
	}

	search() {
		this.network.checkStatus(this);

		let term = this.searchTerm;

		if (term.length > 0) {
			console.log('searching for...' + term)
			// this.showLoader = true
			this.showLoader = true
			this.musics = []
			this.artists = []
			this.showSegmentTab = false;

			// check from local data
			// if nothing is found
			// fetch results from API
			const fetchMusics = this.algolia.searchMusics(term);
			const fetchArtists = this.algolia.searchArtists(term);
			Promise.all([fetchMusics, fetchArtists]).then(response => {
				console.log(response);
				const musics = response[0]['hits'];
				const artists = response[1]['hits'];

				// this.checkResults(res);
				if (musics.length == 0) {
					console.log('no musics pal')
				}

				if (artists.length == 0) {
					console.log('no artists pal')
				}

				if (musics.length == 0 && artists.length == 0) {
					console.log('no musics and no artists pal')
					this.showSegmentTab = false;

					this.alertCtrl.create({
						title: 'Pa gen rezilta!',
						subTitle: '<p>Nou pa rive jwenn rezilta pou rechèch ou a. Tanpri eseye chèche yon lòt mizik oubyen atis.</p>',
						buttons: ['OKE']
					}).present();
				} else {
					let stringRes = JSON.stringify({
						musics, artists, searchTerm: this.searchTerm
					});

					this.storage.set('results', stringRes);

					this.musics = musics;
					this.artists = artists;

					this.showSegmentTab = true;
					this.content.resize();
				}

				this.showLoader = false
			}, (error) => {
				console.log(error)
				this.showLoader = false
				this.onError(error);
			});

			// this.api.search(term).subscribe(res => {
			// 	console.log(res)
			// 	// this.showBreakTags = true;

			// 	// this.checkResults(res);
			// 	if (res.musics.length == 0) {
			// 		console.log('no musics pal')
			// 	}

			// 	if (res.artists.length == 0) {
			// 		console.log('no artists pal')
			// 	}

			// 	if (res.musics.length == 0 && res.artists.length == 0) {
			// 		console.log('no musics and no artists pal')
			// 		this.showSegmentTab = false;

			// 		this.alertCtrl.create({
			// 			title: 'Pa gen rezilta!',
			// 			subTitle: '<p>Nou pa rive jwenn rezilta pou rechèch ou a. Tanpri eseye chèche yon lòt mizik oubyen atis.</p>',
			// 			buttons: ['OKE']
			// 		}).present()
			// 	} else {
			// 		let stringRes = JSON.stringify({
			// 			musics: res.musics,
			// 			artists: res.artists,
			// 			searchTerm: this.searchTerm
			// 		});

			// 		this.storage.set('results', stringRes);

			// 		this.musics = res.musics;
			// 		this.artists = res.artists;

			// 		this.showSegmentTab = true;
			// 	}

			// 	this.showLoader = false
			// }, error =>  {
			// 	this.showLoader = false
			// 	this.onError(error)
			// });
		} else {
			// setTimeout(() => {
			// 	this.toastCtrl.create({
			// 		message: 'Sa w ap chèche a dwe depase 2 karaktè. Ou manke ' + (3 - term.length) + '.',
			// 		duration: 2000,
			// 		position: 'top'
			// 	}).present();
			// }, 1000);
		}
	}

	onCancelOrClear() {
		this.showLoader = false
		// this.getCacheData()
		this.musics = [];
		this.artists = [];
		this.searchTerm = '';
		this.showSegmentTab = false;
		this.showLoader = false;
	}

	goToMusicDetailPage($event, music) {
		console.log(music);
		this.navCtrl.push('MusicDetailPage', { hash: music.hash, music })
	}

	goToArtistDetailPage($event, artist) {
		console.log(artist);
		this.navCtrl.push('ArtistPage', { hash: artist.hash,  artist })
	}

	// checkResults(res) {
	// 	if (res.musics.length == 0) {
	// 		console.log('no musics pal')
	// 	}

	// 	if (res.artists.length == 0) {
	// 		console.log('no artists pal')
	// 	}

	// 	if (res.musics.length === 0 && res.artists.length === 0) {
	// 		console.log('no musics and no artists pal')
	// 		this.showSegmentTab = false;

	// 		this.alertCtrl.create({
	// 			title: 'Pa gen rezilta!',
	// 			subTitle: '<p>Nou pa rive jwenn rezilta pou rechèch ou a. Tanpri eseye chèche yon lòt mizik oubyen atis.</p>',
	// 			buttons: ['OKE']
	// 		}).present();
	// 	} else {
	// 		let stringRes = JSON.stringify({
	// 			musics: res.musics,
	// 			artists: res.artists,
	// 			searchTerm: this.searchTerm
	// 		});

	// 		this.storage.set('results', stringRes)

	// 		this.showSegmentTab = true;
	// 	}
	// }

	onError(error) {
		console.log('error from subscription: ' + error)

		this.alertCtrl.create({
			title: 'Erè chajman rezilta!',
			subTitle: '<p>Nou pa ka rive jwenn rezilta pou rechèch ou a. Ta sanble entènèt ou pa bon.</p>',
			buttons: ['OKE']
		}).present();
	}
}
