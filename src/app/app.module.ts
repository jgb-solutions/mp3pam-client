import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { ApiProvider } from '../providers/api/api';
import { AdsProvider } from '../providers/ads/ads';
import { IonicStorageModule } from '@ionic/storage';
import { AdMobFree } from "@ionic-native/admob-free";
import { StatusBar } from '@ionic-native/status-bar';
import { PushProvider } from '../providers/push/push';
import { AudioProvider } from '../providers/audio/audio';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from '@ionic-native/social-sharing';
import { NetworkProvider } from '../providers/network/network';
import { AlgoliaProvider } from '../providers/algolia/algolia';
import { BackgroundMode } from '@ionic-native/background-mode';
import { LanguageProvider } from '../providers/language/language';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
	declarations: [
		MyApp,
	],
	imports: [
		HttpModule,
		BrowserModule,
		IonicModule.forRoot(MyApp),
		IonicStorageModule.forRoot(),
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp
	],
	providers: [
		Network,
		StatusBar,
		AdMobFree,
    	ApiProvider,
    	AdsProvider,
		SplashScreen,
		InAppBrowser,
    	PushProvider,
		AudioProvider,
		SocialSharing,
		BackgroundMode,
    	LanguageProvider,
    	LocalNotifications,
    	AuthServiceProvider,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
    NetworkProvider,
    AlgoliaProvider,
	],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
