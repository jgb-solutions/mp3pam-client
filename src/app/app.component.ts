import { NetworkProvider } from './../providers/network/network';
import { StatusBar } from '@ionic-native/status-bar';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ApiProvider } from './../providers/api/api';
// import { PushProvider } from './../providers/push/push';
import { BackgroundMode } from '@ionic-native/background-mode';
import { LanguageProvider } from './../providers/language/language';
import { AuthServiceProvider } from './../providers/auth-service/auth-service'
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  	@ViewChild(Nav) nav: Nav

  	rootPage: any = 'HomePage'

  	pages: Array<{title: string, component: any, icon: string}>

  	constructor(
		public events: Events,
		public api: ApiProvider,
		private storage: Storage,
		public platform: Platform,
		public statusBar: StatusBar,
		public lang: LanguageProvider,
		public network: NetworkProvider,
		public auth: AuthServiceProvider,
		public splashScreen: SplashScreen,
		private backgroundMode: BackgroundMode,
		// public push: PushProvider
	) {
		// this.push.local();

    	this.initializeApp()

		// Aplication initialization.
		// this.auth.logout();
		this.checkAuth();

		this.listenToEvents();

		// application's pages
		this.pages = [
			{
				title: lang.get('home'),
				component: 'HomePage',
				icon: 'home'
			},
			{
				title: lang.get('browse_music'),
				component: 'MusicsPage',
				icon: 'musical-notes'
			},
			{
				title: lang.get('upload_music'),
				component: 'UploadMusicPage',
				icon: 'cloud-upload'
			},
			{
				title: lang.get('categories'),
				component: 'CategoriesPage',
				icon: 'folder'
			},
			{
				title: lang.get('about'),
				component: 'AboutPage',
				icon: 'information-circle'
			},
			{
				title: lang.get('help'),
				component: 'HelpPage',
				icon: 'help'
			},

		]
  	}

	initializeApp() {
		this.platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.statusBar.styleDefault()
			this.splashScreen.hide()

			// Activate Background Mode
			this.backgroundMode.enable();

			// watch the network status
			this.network.watchNetworkStatus();
		});
	}

	checkAuth() {
		this.storage.get('userInfo').then(userInfo => {
			if (userInfo) {
				let jsonUserInfo = JSON.parse(userInfo);
				this.auth.loggedIn = true;
				this.auth.user = jsonUserInfo.user;
				this.api.userToken = jsonUserInfo.token;
			} else {
				console.log('user not logged in')
				this.rootPage = 'LoginPage';
			}
		}, error => {
			console.log('could not load the value from storage');
		});
	}

	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}

	logout() {
		this.nav.setRoot('LoginPage');
		this.auth.logout();
	}

	openProfilePage() {
		this.nav.push('UserPage');
	}

	listenToEvents() {
		this.events.subscribe('loginRequired', () => {
			this.auth.logout()
			this.nav.setRoot('LoginPage', {
				errorMessage: 'Sesyon w lan sanble ekspire. Rekonekte w ankò pou nou kapab idantifye w.'
			})
		})

		this.events.subscribe('passwordChanged', () => {
			this.nav.setRoot('LoginPage', {
				successMessage: 'Ou reyinisyalize modpas ou a avèk siksè. Ou kapab konekte w ak nouvo modpas ou a kounye a.'
			});
		});
	}
}
