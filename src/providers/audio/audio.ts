import { Injectable } from '@angular/core';
import { Events, ToastController } from 'ionic-angular';

@Injectable()
export class AudioProvider {
	audio: any;
	position: number;
    elapsed: string = '';
    duration: string = '';
    // isPaused: boolean = true;
    onRepeat: boolean = false;
	isShuffled: boolean = false;
    isPlaying: boolean = false;
    currentTrack: any;
    playedTracks: any[] = [];

	constructor(
        private events: Events,
        private toast: ToastController
	) {
		// Initialize an Audio object
        this.audio = new Audio();

        // fetch current track from local storage
        this.currentTrack = {
            title : "Bad News",
            detail : "Cat, 'if you don't explain it is right?' 'In my youth,' Father William replied to his ear. Alice considered a little, and then said 'The fourth.' 'Two days wrong!' sighed the Lory, as soon as she.",
            lyrics : "She went in without knocking, and hurried off at once, while all the jurymen are back in a minute or two, they began moving about again, and looking at them with large round eyes, and half believed herself in Wonderland, though she knew that it might tell her something worth hearing. For some minutes it puffed away without speaking, but at the bottom of the court. 'What do you mean \"purpose\"?' said Alice. 'Then you should say what you would seem to come once a week: HE taught us Drawling, Stretching, and Fainting in Coils.' 'What was THAT like?' said Alice. 'Well, I never heard of \"Uglification,\"' Alice ventured to ask. 'Suppose we change the subject,' the March Hare and the Mock Turtle, 'but if they do, why then they're a kind of serpent, that's all I can say.' This was such a puzzled expression that she was now only ten inches high, and her eyes filled with tears again as she could. 'The game's going on between the executioner, the King, 'or I'll have you executed, whether you're a.",
            url : "http://192.168.43.102:8000/api/v1/musics/42139505",
            play_count : 0,
            play_url: "https://files.mp3pam.com/file/mp3pam/(Rington)+OMVR+-+Bad+News+-+RingtonOrg.mp3",
            download_count : 0,
            download_url : "http://192.168.43.102:8000/t/42139505",
            image : "http://mp3pam-server.test/assets/images/OMVR-Bad-News-2016-2480x2480.jpg",
            category :  {
                name : "Konpa",
                slug : "konpa",
                url : "http://192.168.43.102:8000/api/v1/categories/konpa"
            },
            artist: {
                avatar:	"http://192.168.43.102:8000/assets/images/logo.jpg",
                bio: null,
                musics:	"http://192.168.43.102:8000/api/v1/artists/77868635/musics",
                name:	"OMVR",
                stageName:	"OMVR",
                url:	"http://192.168.43.102:8000/api/v1/artists/77868635",
                verified : false
            }
        }

		// Start listening to events
        this.listenToAudioEvents();
        this.listenToEvents();
	}

    onEnded(e) {
        console.log('ended');
        this.isPlaying = false;
    }

    onTimeUpdate(e) {
        const elapsed = this.audio.currentTime;
        const duration = this.audio.duration;
        this.position = elapsed / duration;
        this.elapsed = this.formatTime(elapsed);

        this.duration = duration > 0 ? this.formatTime(duration) : '';

    }

	play(url?: string): void {
        this.prepare(url);
        this.audio.play().then(() => {
            console.log('started playing...');
        }, error => {
            console.log('failed because ' + error);
            let toast = this.toast.create({
                message: 'Nou pa rive jwe mizik la. Tanpri eseye ankò.',
                duration: 3000,
                closeButtonText: 'OKE'
            });

            toast.present();
        });
    }

    resume(): void {
        // this.isPaused = false;
        this.audio.play();
        console.log('resuming...');
    }

    playOrResume(): void {
        if (this.audio.paused && this.audio.currentTime > 0) {
            this.resume();
        } else {
            this.play(this.currentTrack.play_url);
        }
    }

	pause() {
       this.audio.pause();
       console.log('pausing...');
    }

    // stop() {
    //     this.audio.pause();
    //     this.audio.currentTime = 0;
    //     // this.isPaused = false;
    // }

    previous() {
        if (this.isShuffled) {
            this.playCurrentTrack(this.randomTrack());
        } else {
            let indexToPlay;
            let totalTracksIndexes = this.playedTracks.length - 1;
            let currentIndex = this.findIndex(this.currentTrack);

            if (currentIndex > 0) {
                indexToPlay = currentIndex - 1;
            } else {
                indexToPlay = totalTracksIndexes;
            }

            this.playCurrentTrack(this.playedTracks[indexToPlay]);
        }
	}

	next(): void {
        if (this.isShuffled) {
            this.playCurrentTrack(this.randomTrack());
        } else {
            let indexToPlay;
            let totalTracksIndexes = this.playedTracks.length - 1;
            let currentIndex = this.findIndex(this.currentTrack);

            if (currentIndex < totalTracksIndexes) {
                indexToPlay = currentIndex + 1;
            } else {
                indexToPlay = 0;
            }

            this.playCurrentTrack(this.playedTracks[indexToPlay]);
        }
    }

	adjustVolume() {

	}

    // backward() {
    //     const elapsed = this.audio.currentTime;
    //     console.log(elapsed);
    //     if (elapsed >= 5) {
    //         this.audio.currentTime = elapsed - 5;
    //     }
    // }

    // forward() {
    //     const elapsed = this.audio.currentTime;
    //     const duration = this.audio.duration;
    //     if (duration - elapsed >= 5) {
    //         this.audio.currentTime = elapsed + 5;
    //     }
    // }

    randomTrack(): any {
        return this.playedTracks[Math.floor(Math.random() * this.playedTracks.length)];
    }

    formatTime(seconds): string {
        let minutes: any = Math.floor(seconds / 60);
        minutes = (minutes >= 10) ? minutes : '0' + minutes;
        seconds = Math.floor(seconds % 60);
        seconds = (seconds >= 10) ? seconds : '0' + seconds;
        return minutes + ':' + seconds;
    }

    playCurrentTrack(track): void {
        this.currentTrack = track;
        this.play(track.play_url);
    }

    // findTracks(value) {
    //     return this.apiService.get(`${this.apiService.prepareUrl('https://api.soundcloud.com/tracks')}&q=${value}`, false)
    //         .debounceTime(300)
    //         .distinctUntilChanged()
    //         .map(res => res.json());
    // }

	onTrackFinished(track: any): void {
		console.log('Track finished', track)
	}

    prepare(url: string): void {
        this.audio.src = url;
        this.audio.load();
    }

	listenToAudioEvents() {
		this.audio.onended = this.onEnded.bind(this);
        this.audio.ontimeupdate = this.onTimeUpdate.bind(this);
    }

    listenToEvents(): void {
		this.events.subscribe('playTrack', track => {
            console.log('Playing track', track.title);
            this.playCurrentTrack(track);
            this.addTrack(track);
		});

		this.events.subscribe('pauseTrack', () => {
			console.log('Pausing track')

            this.pause();
            console.log('track paused');
		});

		// this.events.subscribe('stopTrack', (music) => {
		// 	console.log('Stopping track', music)

		// 	this.stop.stop(0)
		// });
	}

    addTrack(track): void {
        if (!this.contains(track)) {
            console.log('not here');
            this.playedTracks.push(track);
        }

        console.table(this.playedTracks);
    }

    contains(currentTrack): boolean {
        for (let i in this.playedTracks) {
            if(this.playedTracks[i] === currentTrack) {
                return true;
            }
        }

        return false;
    }

    findIndex(currentTrack): number {
        return this.playedTracks.findIndex(track => {
            return this.currentTrack === track;
        });
    }
}
