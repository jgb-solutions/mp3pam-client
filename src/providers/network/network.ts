import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { LanguageProvider } from './../language/language';
import { Platform, ToastController } from 'ionic-angular';

@Injectable()
export class NetworkProvider {
	connected: Subscription;
	disconnected: Subscription;

	constructor(
	private network: Network,
	private platform: Platform,
	private lang: LanguageProvider,
	private toast: ToastController,
	) {
	  this.platform.ready().then(() => {
	  });
	}

	watchNetworkStatus(){
	  this.connected = this.network.onConnect().subscribe((data) => {
		console.log(data);
		this.displayNetworkUpdate('isOnline');
	  }, error => {
		console.log(error);
	  });

	  this.disconnected = this.network.onDisconnect().subscribe(data => {
		console.log(data);
		this.displayNetworkUpdate('isOffline');
	  }, error => {
		console.log(error);
	  })
	}

	displayNetworkUpdate(state: string): void {
		this.toast.create({
		message: this.lang.get(state),
		duration: 4000
		}).present();
	}

	isOnline(): boolean {
		return this.network.type !== 'none';
	}

	isOffline(): boolean {
		return this.network.type === 'none';
	}

	// check if there's internet because making any request
	checkStatus(object, infiniteScroll?) {
		if (!navigator.onLine) {
		// if (this.isOffline) {
			object.showLoader = false;
			this.displayNetworkUpdate('isOffline');

			if (infiniteScroll) {
				infiniteScroll.complete();
			}

			return;
		}

	}

	// ionViewDidLeave(){
	//   this.connected.unsubscribe();
	//   this.disconnected.unsubscribe();
	// }
}
