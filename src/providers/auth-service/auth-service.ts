import { Storage } from '@ionic/storage';
import { ApiProvider } from './../api/api';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthServiceProvider {
	loggedIn: boolean = false;
	user: any;

	constructor(
		public api: ApiProvider,
		private storage: Storage
	) {}

	isLoggedIn() {
		return this.loggedIn;
	}

	loginWith(response) {
		this.loggedIn = true
		this.user = response.user
		this.api.userToken = response.token

		this.storage.set('userInfo', JSON.stringify({
			user: response.user,
			token: response.token
		}));
	}

	loginWithFacebook(apiResponse) {
		let response = JSON.parse(apiResponse);

		this.user = response.user
		this.api.userToken = response.token
		this.loggedIn = true

		this.storage.set('userInfo', JSON.stringify({
			user: response.user,
			token: response.token
		}));
	}

	logout() {
		this.storage.remove('userInfo');
		
		this.loggedIn = false
		this.user = null
		this.api.userToken = null
	}
}
