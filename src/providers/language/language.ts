import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { haitians } from './ht';

@Injectable()
export class LanguageProvider {
	defaultLanguage: string = 'ht';
	currentLanguage: string;
	availableLanguages: object = {
		ht: 'Kreyòl',
		fr: 'Français'
	};

	languageStrings: object = {
		ht: haitians,
		fr: {}
	}

	constructor(
		private storage: Storage
	) {
		this.setCurrentLanguage();
	}

	setCurrentLanguage() {
		this.currentLanguage = this.defaultLanguage;
		this.storage.get('currentLanguage').then(currentLanguage => {
			if (currentLanguage) {
				this.currentLanguage = currentLanguage;
			} else {
				this.currentLanguage = this.defaultLanguage;
			}
		}, error => console.error(error));
	}

	get (key: string): string {
		return this.languageStrings[this.currentLanguage][key] || key;
	}

	set (lang: string): void {
		this.currentLanguage = lang;

		this.storage.set('currentLanguage', lang);
	}
}
