import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Platform, AlertController } from 'ionic-angular';

@Injectable()
export class PushProvider {

	constructor(
		private localNotifications: LocalNotifications,
		private platform: Platform,
		private alertCtrl: AlertController
	) {
		platform.ready().then((readySource) => {
			localNotifications.on('click', (notification, state) => {
				let data: any = JSON.parse(notification.data);

				let alert = alertCtrl.create({
					title: notification.title,
					subTitle: data.subTitle
				});

				alert.present();
			});
		});
	}

	local() {
		// this.localNotifications.clearAll();

		this.localNotifications.schedule({
			id: 123,
			title: "Icon Notif",
			text: "local from push provider",
			// sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
			data: { subTitle: 'my subtile' },
			// every: 'minute',
			// icon: 'http://192.168.43.104:8000/assets/images/logo-trans.png'
			icon: "res://icon.png",
          	smallIcon:"res://icon.png"
		});
	}

}
