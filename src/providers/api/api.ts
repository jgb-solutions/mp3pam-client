import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/do'
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Events, Platform } from 'ionic-angular';
import { NetworkProvider } from './../network/network';
import { Http, Headers, Response } from '@angular/http';

@Injectable()
export class ApiProvider {
	userToken: string;
	apiUrl: string = '/api/v1';
	facebookAuthURL = this.apiUrl + "/auth/facebook";
	facebookAuthHandleURL = this.facebookAuthURL + "/handle";

  	constructor(
		public http: Http,
		public events: Events,
		public network: NetworkProvider,
		public platform: Platform,
	) {
		this.getAuthToken()

		// check the platform and assign the correct API url
		if (platform.is('cordova')) {
			this.apiUrl = "http://192.168.43.102:8000/api/v1";
			this.facebookAuthURL = "http://192.168.43.102:8000/api/v1/auth/facebook"
		}
  	}

	// Musics
	getMusics(url?) {
		if (! url) {
			url = this.apiUrl + '/musics'
		}

		return this.makeRequest(url, 'get')
	}

	getMusic(url) {
		return this.makeRequest(url, 'get')
	}

	search(term) {
		let url = this.apiUrl + '/search?term=' + term
		return this.makeRequest(url, 'get')
	}

	// Categories
	getCategories() {
		let url = this.apiUrl + '/categories'
		return this.makeRequest(url, 'get')
	}

	getMusicsByCategory(url) {
		return this.makeRequest(url, 'get')
	}


	// Authentication
	getAuthToken() {
		return this.userToken
	}

	login(credentials) {
		let url = this.apiUrl + '/login'
		return this.makeRequest(url, 'post', credentials)
	}

	register(credentials) {
		let url = this.apiUrl + '/register'
		return this.makeRequest(url, 'post', credentials)
	}

	// Password Recovery
	recover(credentials) {
		let url = this.apiUrl + '/recover'
		return this.makeRequest(url, 'post', credentials)
	}

	verify(credentials) {
		let url = this.apiUrl + '/verify'
		return this.makeRequest(url, 'post', credentials)
	}

	changePassWord(credentials) {
		let url = this.apiUrl + '/reset-password'
		return this.makeRequest(url, 'post', credentials)
	}

	// small API for sending all API requests.
	makeRequest(url, verb, data = null) {
		let headers = new Headers()
		headers.append('Authorization', 'Bearer ' + this.userToken)
		// headers.append('Content-Type', 'application/json charset=UTF-8')
		headers.append('X-Requested-With', 'XMLHttpRequest')


		let request
		let options = { headers }

		switch (verb) {
			case 'get':
				request = this.http.get(url, options)
				break
			case 'post':
				request = this.http.post(url, data, options)
				break
			case 'put':
				request = this.http.put(url, data, options)
				break
			case 'delete':
				request = this.http.delete(url, options)
				break
		}

		return request.map(res => res.json())
			.catch((response: Response) => {
				switch (response.status) {
					case 400:
						this.events.publish('loginRequired');
						break;
					// case 500:
					// 	console.log('server error');
					// 	return;
				}

				console.log(response.json())

				let res = {
					message: response.json().message,
					status: response.status
				};

				return Observable.throw(res);
			});
	}

	makeUrlFor(resourceName, resourceId) {
		let url;

		switch (resourceName) {
			case 'music':
				url = this.apiUrl + '/musics/' + resourceId;
				break;
			case 'category':
				url = this.apiUrl + '/categories/' + resourceId;
				break;
		}

		return url;
	}
}
