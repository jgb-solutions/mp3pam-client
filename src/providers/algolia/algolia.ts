import { Injectable } from '@angular/core';
import algoliasearch, { AlgoliaClient, AlgoliaIndex } from 'algoliasearch';

@Injectable()
export class AlgoliaProvider {
	client: AlgoliaClient;
	musicIndex: AlgoliaIndex;
	artistIndex: AlgoliaIndex;
	appID: string = 'AILJ0MY0ZX';
	apiKey: string = '9aff1789c01168e609edb39066efa2a7';

	constructor() {
		this.client = algoliasearch(this.appID, this.apiKey);
		this.musicIndex = this.client.initIndex('musics');
		this.artistIndex = this.client.initIndex('artists');
	}

	searchMusics(term: string) {
		return this.search(term, 'music');
	}

	searchArtists(term: string) {
		return this.search(term, 'artist');
	}

	search(term: string, type) {
		return new Promise((resolve, reject) => {
			return this[type + 'Index'].search(term, (err, response) => {
				if (err) {
					reject(err);
				}

				resolve(response);
			});
		});
	}
}