import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';

import {
	AdMobFree,
	AdMobFreeBannerConfig,
	AdMobFreeInterstitialConfig,
	AdMobFreeRewardVideoConfig
} from "@ionic-native/admob-free";


@Injectable()
export class AdsProvider {
	currentPlatform: string;

	adMobInfo: any = {
		android:  {
			id: 'ca-app-pub-3793163111580068~6027793241',
			bannerID: 'ca-app-pub-3793163111580068/2308395307',
			interstitialID: 'ca-app-pub-3793163111580068/5305259589',
			rewardID: 'ca-app-pub-3793163111580068/5389908110'
	
		},
		ios: {
			id: 'ca-app-pub-3793163111580068~2201063021',
			bannerID: 'ca-app-pub-3793163111580068/8738551212',
			interstitialID: 'ca-app-pub-3793163111580068/2967705002',
			rewardID: 'ca-app-pub-3793163111580068/1079908266'
		}
	}

	constructor(
		public adMobFree: AdMobFree,
		private platform: Platform
	) {
		if (platform.is('android')) {
			this.currentPlatform = 'android';
		} else if (platform.is('ios')) {
			this.currentPlatform = 'ios';
		}
	}

	async prepareBannerAd() {
		try {
			const bannerConfig: AdMobFreeBannerConfig = {
			//   id: this.adMobInfo[this.currentPlatform].bannerID,
			  isTesting: true,
			  autoShow: false
			}
	  
			this.adMobFree.banner.config(bannerConfig);
	  
			const result = await this.adMobFree.banner.prepare();
			console.log(result);
		  } catch (e) {
			console.error(e);
			alert(e);
		}
	}

	showBannerAd() {
		this.prepareBannerAd().then(() =>  this.adMobFree.banner.show());
	}

	hideBannerAd() {
		this.adMobFree.banner.hide();
	}
	
	async prepareInterstitialAd() {
		try {
		  const interstitialConfig: AdMobFreeInterstitialConfig = {
			// id: this.adMobInfo[this.currentPlatform].interstitialID,
			isTesting: true,
			autoShow: true
		  }
	
		  this.adMobFree.interstitial.config(interstitialConfig);
	
		  const result = await this.adMobFree.interstitial.prepare();
		  console.log(result);
		} catch (e) {
		  console.error(e)
		}
	}

	showInterstitialAd() {
		this.prepareInterstitialAd().then(() => this.adMobFree.interstitial.show());
	}

	async prepareRewardVideoAd() {
		try {
		  	const videoRewardsConfig: AdMobFreeRewardVideoConfig = {
				// id: this.adMobInfo[this.currentPlatform].rewardID,
				isTesting: true,
				autoShow: true
		  	}
	
			this.adMobFree.rewardVideo.config(videoRewardsConfig);
		
			const result = await this.adMobFree.rewardVideo.prepare();
			console.log(result);
		} catch (e) {
		  console.error(e);
		}
	}

	showRewardVideoAd() {
		this.prepareRewardVideoAd().then(() => this.adMobFree.rewardVideo.show());
	}
}
