import { AbstractControl } from '@angular/forms'

export class PasswordValidators {
    static Confirmation(Control: AbstractControl) {
        console.log(Control.root.get('password'), Control.value)

        let password = Control.root.get('password')

        if (Control.value) {
            if(Control.value !== password.value) {
                console.log('false');
                Control.setErrors({nomatch: true})
                return {nomatch: 'the passwords did not match'}
            } else {
                console.log('true');
                return null
            }
        }
    }
}