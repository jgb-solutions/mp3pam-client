export interface CategoryInterface {
	name: string,
	slug: string,
	url: string
}