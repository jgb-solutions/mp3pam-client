export interface ArtistInterface {
    avatar: string,
    bio: string,
    hash: number,
    name: string,
    stageName: string
}