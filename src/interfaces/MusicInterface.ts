import { CategoryInterface } from './CategoryInterface';
import { ArtistInterface } from './ArtistInterface';

export interface MusicInterface {
	artist: ArtistInterface,
	category: CategoryInterface,
	detail: number,
	download_count: number,
	download_url: string,
	hash: number,
	image: string,
	lyrics: string,
	play_count: number,
	play_url: string,
	publicUrl: string,
	title: string,
	url: string
}